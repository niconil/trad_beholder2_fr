#!/usr/bin/python3

import re
from xml.etree import ElementTree

def devine_langue(chaine):
    langue = "FR"
    mots = chaine.split(' ')
    for cle in ['the', 'The', 'to', 'at', 'you', 'of', 'about']:
        if cle in mots:
            langue = "EN"
            break
    return langue


#lecture du fichier anglais (fichier de reference)
fic_en = "strings.xml"

order_en = []
dico_en = {}

with open(fic_en,'r') as src:
    XmlFile = ElementTree.parse(src)
    Rec = XmlFile.findall('string')
    for phrase in Rec:
        id = phrase.get('id')
        order_en.append(id)
        dico_en[id] = phrase.text

#le dico anglais est chargé
fic_fr = "test_FR.xml"

dico_fr = {}

#au prealable le fichier FR doit être valide
#paquet libxml2-utils
#xmllint --schema strings.xsd --noout test_FR.xml 

with open(fic_fr,'r') as src:
    XmlFile = ElementTree.parse(src)
    Rec = XmlFile.findall('string')
    for phrase in Rec:
        id = phrase.get('id')
        if id in dico_fr:
            print("%5s : id en doublon" % id)
            #on passe
            # continue
        if re.match( "string", phrase.text):
            print("%5s valeur suspecte : '%s'" % (id, phrase.text))
            continue
        #on a passé les tests, l'élément est ajouté
        dico_fr[id] = phrase.text

#on reconstitue un fichier opérationnel ...
proposition_fr = {}
compteur        = 0
compteur_absent = 0
liste_absent = []
for id in order_en:
    if id not in dico_fr:
        print("%5s absent de la traduction '%s'" % (id, dico_en[id]))
        compteur_absent += 1
        if re.match(r"[\s\n]+$", dico_en[id]) is None:
            liste_absent.append(id)
        proposition_fr[id] = "%s" % dico_en[id]
        continue
    proposition_fr[id] = dico_fr[id]
    compteur += 1
print( "compteur : %s" % compteur)
print( "absent(s): %s" % compteur_absent)

#ecriture de la proposition sur disque
c = { "FR":0, "EN": 0}
resultat_fr = "strings_FR.xml"
entete_fr = [
    '<?xml version="1.0" encoding="UTF-8"?>',
    '<!--beholder strings table. DO NOT edit it by hand!-->',
    '<data>',
    '  <UTF8 />'
]
with open(resultat_fr,'w') as out:
    #on écrit l'entete
    for ligne in entete_fr:
        out.write("%s\n" % ligne)
    #on écrit toutes les 'string'
    for id in proposition_fr:
        #petit traitement sur le contenu de proposition pour remplacer certains symboles
        chaine_normalisee = proposition_fr[id]
        if id in liste_absent:
            langue = "EN"
        else:
            langue = devine_langue( chaine_normalisee)
        c[langue] += 1
        chaine_normalisee = chaine_normalisee.replace( '&', '&amp;')
        chaine_normalisee = chaine_normalisee.replace( '>', '&gt;')
        chaine_normalisee = chaine_normalisee.replace( '<', '&lt;')
        out.write('  <string l="%s" id="%s">%s</string>%s' % (langue, id, chaine_normalisee, "\n"))
    #on cloture le fichier
    out.write('</data>')
#c'est prêt !
print(c)


