#!/usr/bin/python3

import re
import os
from xml.etree import ElementTree

fic_fr = "../strings_FR_phase2.xml"
order = []
liste_en = []
dico_en = {}

with open(fic_fr,'r') as src:
    XmlFile = ElementTree.parse(src)
    Rec = XmlFile.findall('string')
    for phrase in Rec:
        id = phrase.get('id')
        langue = phrase.get('l')
        if langue=='EN':
            dico_en[id] = phrase.text
            liste_en.append(int(id))
        order.append(id)

#split par centaine
for centaine in range(200):
    liste_cent = []
    for indice in range(100):
        numero = centaine * 100 + indice
        string_id = str(numero)
        if string_id in dico_en:
            liste_cent.append("<%s>%s" % (string_id,dico_en[string_id]))
    fic_centaine = "../a_traduire/a_traduire_%03d.txt" % centaine
    if liste_cent:
        with open(fic_centaine,'w') as out:
            for ligne in liste_cent:
                out.write("%s\n" % ligne)
    else:
        try:
            os.remove(fic_centaine)
        except FileNotFoundError:
            pass

