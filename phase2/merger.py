#!/usr/bin/python3

import os
import re
import glob
from xml.etree import ElementTree

fic_fr = "../strings_FR_phase2.xml"
order = []
dico = {}
langue = {}

with open(fic_fr,'r') as src:
    XmlFile = ElementTree.parse(src)
    Rec = XmlFile.findall('string')
    for phrase in Rec:
        id = phrase.get('id')
        l = phrase.get('l')
        dico[id] = phrase.text
        langue[id] = l
        order.append(id)

#maintenant on lit tout les fichiers du repertoire cible
repertoire_cible = "../a_traduire"
dico_string_FR = {}
liste_fic = glob.glob("%s/traduit_*.txt" % repertoire_cible)
for fic in liste_fic:
    print("analyse du fichier : '%s'" % fic)
    with open(fic,'r') as src:
        lignes = src.readlines()
    #a partir du tableau des lignes on doit reconstituer le tableau des strings
    s = ""
    for l in lignes:
        m = re.match(r"<(\d{1,5})>(.*)$", l)
        if m:
            #c'est une nouvelle chaine
            #si il y a une chaine précédente
            if s:
                dico_string_FR[id_string] = s[:-1]
            id_string = m.group(1)
            s = m.group(2)+"\n"
        else:
            s += "%s" % l
    else:
        if s:
            dico_string_FR[id_string] = s[:-1]
    #suppression du fichier
    try:
        os.remove(fic)
    except FileNotFoundError:
        pass

#on a chargé les traduction dans dico_string_FR

#on va batir une proposition

#on reconstitue un fichier opérationnel ...
proposition_fr = {}
compteur        = 0
reste_a_traduire = 0
for id in order:
    if id in dico_string_FR:
        #on rempace par la nouvelle traduction
        proposition_fr[id] = dico_string_FR[id]
        langue[id] = "FR"
        compteur += 1
    else:
        proposition_fr[id] = dico[id]
    if langue[id] == "EN":
        reste_a_traduire += 1
print( "%s chaînes insérées dans le fichier global" % compteur)
print( "Nombre de chaînes restant à traduire : %s" % reste_a_traduire)

#Ecriture de la proposition sur disque
resultat_fr = "../strings_FR_phase2.xml"
entete_fr = [
    '<?xml version="1.0" encoding="UTF-8"?>',
    '<!--beholder strings table. DO NOT edit it by hand!-->',
    '<data>',
    '  <UTF8 />'
]
with open(resultat_fr,'w') as out:
    #on écrit l'entete
    for ligne in entete_fr:
        out.write("%s\n" % ligne)
    #on écrit toutes les 'string'
    for id in proposition_fr:
        #petit traitement sur le contenu de proposition pour remplacer certains symboles
        chaine_normalisee = proposition_fr[id]
        l = langue[id]
        chaine_normalisee = chaine_normalisee.replace( '&', '&amp;')
        chaine_normalisee = chaine_normalisee.replace( '>', '&gt;')
        chaine_normalisee = chaine_normalisee.replace( '<', '&lt;')
        out.write('  <string l="%s" id="%s">%s</string>%s' % (l, id, chaine_normalisee, "\n"))
    #on cloture le fichier
    out.write('</data>')
#c'est prêt !
