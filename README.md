# trad_beholder2_FR

contribution à la francisation du jeu Beholder 2

# phase2

ça devrait sensiblement s'accélérer
on se place dans le répertoire phase2

il y a deux scripts python
- splitter
- merger

Avec le splitter on va générer des fichiers de string a traduire les chaines sont regroupés par id ayant la meme centaine et générer des fichiers dans le répertoire 'a_traduire'

par exemple :
a_traduire/a_traduire_139.txt
contient toutes les chaines en anglais à traduire avec un id de la forme 139xx
(de 13900 à 13999)

donc pour lancer le splitter
cd phase2
./splitter.py

on traduit le fichier (complètement ou partiellement)
FAIRE ATTENTION de ne laisser que des chaines françaises dans ce fichier traduit
que l'on laisse dans ce repertoire à traduire (mais qu'il faut renommer traduit_139.txt)

par exemple
a_traduire/a_traduire_122.txt une fois traduit doit etre renommé en 
a_traduire/traduit_122.txt (et ne contenir que des chaines françaises, les chaines dont la traduction pose un pb peuvent être enlevées, on y reviendra ultérieurement)

une fois fait, on va regénérer le fichier complet utilisable par le jeu
pour cela on lance le merger :

cd phase2
./merger.py

les chaines traduites sont intégrées et on peut effacer le fichier de traduction
rm a_traduire/traduit_122.txt (il ne sert plus il a été intégré)

le fichier global s'appelle
strings_FR_phase2.xml
les scripts de la phase2 travaille avec celui là pour ne pas interférer avec les travaux précédents

# travaux initiaux
dans le repertoire 'tools':
  strings.xml : le fichier des chaines en langue anglaise
  test_FR.xml : le fichier récupéré auprès des premiers volontaires ayant entamé la traduction (le fichier a été subi quelque modifications, pour éliminer des erreurs de syntaxes)
  (on a perdu la colorisation des chaines marquées rouge ou verte selon l'état de traduction)
  strings.xsd : le schéma ayant permis de controler la validité syntaxique du fichier test_FR.xml

  assistant_trad.py: script rebatissant un fichier de traduction
  - à partir des chaines attendus dans le langage pris en référence (anglais)
  - en supprimant les doublons du fichier (test_FR)
  - en ajoutant les id manquants

le fichier résultat généré est "pas trop difficile à injecter dans le jeu" steam
(un petit attribut pour chaque chaine a été rajouté dans le fichier pour savoir si la chaine a été traduite ou pas)

le fichier produit par le script python est strings_FR.xml

pour que le travail continue
il faut repartir de cette base et maintenant continuer à améliorer le fichier suivant:

strings_FR_beholder2.xml

- au 28/08/2019 il resterait environ : 5300 chaines à traduire (sur environ 11850)

# comment utiliser ce fichier avec steam

en attendant une solution plus intégrée, il faut s'inspirer de ce mode opératoire :
https://beholder2.com/steamworkshop
(je suis sous linux, mes explications ne pourront être insuffisantes pour un environnement windows mais avec vos contributions on devrait pouvoir y arriver)

1 - lancer Beholder 2 dans le mode permettant de modifier les chaines de texte

s'inspirer de l'image ...
https://static.tildacdn.com/tild6265-3565-4232-b431-396137663063/334.jpg

dans les propriétés du jeu sous steam chercher à rajouter une option de lancement
dans la petite popup comme indiquer mettre :
--runtimetextedit

2 - dans le jeu activer le mode edition de texte

si l'atape précédente à fonctionnée il doit se passer qq chose qaund vous appuyez sur
Ctrl + Shift + L

une popup apparait avec l'ensemble des chaines de l'écran en cours

3 - appuyez sur le bouton langage

toujours dans cette popup cliquer sur le boyuton 'langage'. la liste des 'langues disponibles' doit alors s'afficher

4 - maintenant on va ajouter une version correspondant au français

- appuyer sur 'new language'
on choisit le langage de départ (prendre l'anglais)
on entre le nom du langage 'Francais'
on entre le nom de la langue système 'French'
et le langage code 'fr-FR'
et appuyez sur le bouton create

une fois créer vous désignez cette langue comme langue 'active' en cochant la case idoine
https://static.tildacdn.com/tild3562-3162-4536-b765-613634646665/345.JPG

vous appuyez sur le bouton 'open folder'

vous devez voir un fichier 'png' et un fichier 'vdf'
vous devez voir un nouveau répertoire créer avec un id (code hexadéciaml d'environ 25 caractères)
le fichier important est le fichier strings.xml qui se trouve dans le répertoire nouvellement créé

5 - utiliser la traduction partielle

c'est à vous de récupérer le fichier en cours de traduction
dispo ici :
https://gitlab.com/niconil/trad_beholder2_fr/raw/master/strings_FR_phase2.xml

et de le mettre en lieu et place du fichier strings.xml

en relançant le jeu ça devrait être bon
(c'est à dire vous devriez avoir la version partiellement traduite)

A titre d'exemple, dans mon environnement 
(steam sur linux, mon fichier se trouve ici :
~/.steam/steam/steamapps/common/Beholder\ 2/Beholder2_Data/StreamingAssets/Localization/6e4cc135-2068-4ba7-83f9-c0bc0b514804/strings.xml)

j'espère que cela vous aura été utile ...






 


